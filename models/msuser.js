const bcrypt = require('bcrypt');

const passport = require('passport');

//jwt auth
//const jwt = require('jsonwebtoken');

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class msuser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    // Method untuk melakukan enkripsi
    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    // Lalu, kita buat method register
    static registerUser = ({iduser,name,password,level,status}) => {
      
      const encryptedPassword = this.#encrypt(password);

      /*
      #encrypt dari static method
      encryptedPassword akan sama dengan string
      hasil enkripsi password dari method #encrypt
      */

      return this.create({
        iduser,
        name,
        password: encryptedPassword,
        level,
        status
      })
    } 

    checkPassword = password => bcrypt.compareSync(password, this.password);

    /*
    generateToken = () => {
      const payload = {
        id: this.id,
        name: this.name
      }

      const KEY = `Jonathan`;
      const token = jwt.sign(payload, KEY);

      return token;
    }*/

    static authenticate = async ({name, password}) => {
      try {
        const user = await this.findOne({ where: { name } });
        if(!user) return Promise.reject("User Not Found!!");
        const isPasswordValid = user.checkPassword(password);
        if(!isPasswordValid) return Promise.reject("wrong password");
        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error)
      }
    }
    


  };

  msuser.init({
    iduser: DataTypes.STRING,
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    level: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'msuser',
  });
  return msuser;
};