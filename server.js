//untuk panggil expressnya
const express = require('express');

//untuk membuat session agar user diharuskan login terlebih dahulu
const session = require('express-session')

/*
dipakai untuk redirect ke halaman
selanjutnya dan menunjukkan pesan tertentu seperti pesan sukses atau pesan
error
*/
const flash = require('express-flash')

const app = express();
const port = 5700;

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());


app.use(express.static('public'));

// create session
// Kedua, setting session handler
app.use(session({
    secret: 'Jonathan',
    resave: false,
    saveUninitialized: false
  }))



// Ketiga, setting passport
// (sebelum router dan view engine)

//local auth
const passport = require('./lib/passportLocal')

//jwt auth
//const passport = require('./lib/passport')
app.use(passport.initialize())
app.use(passport.session())

// Keempat, setting flash
app.use(flash())

// Kelima, setting view engine
app.set('view engine', 'ejs');






//serving data dalam bentuk json
//const dataUser = require('./db/user.json');

//using controllers login
//const { loginSuccess, loginForm, loginCheck, loginIndex } = require('./controllers/loginController');

const { register } = require('./controllers/loginController');


//using controller user
const { indexUser, createUser, viewUser, deleteUser, updateUserGet, updateUserPost } = require ('./controllers/userController');

    /*

    app.get('/user', (req, res) => {

      if (req.session.loggedIn) {

        res.status(200).json(dataUser); 

      } else {

        res.redirect('/login')

      }

    } ); */

/* === MSUSER === */

app.get('/create', indexUser);
app.get('/update/:iduser', updateUserGet);
app.get('/view', viewUser);
app.get('/delete/:iduser', deleteUser);
app.post('/create', createUser);
app.post('/update/updateuser/:iduser', updateUserPost );


/* === LOGIN === */

//app.get('/login', loginForm);
//app.post('/auth', loginCheck );
//app.get('/homelogin', loginSuccess);
//app.get('/homepage', loginIndex);





/*  ==================== API ====================*/

//connection API
const userAPIRouter = require('./router/userAPIRouter');
app.use(userAPIRouter);

//change data to JSON type
app.use(express.json());

//untuk notifikasi node.js sudah berhasil jalan
app.listen(port, () => console.log(`App sudah berjalan di port ${port}`));