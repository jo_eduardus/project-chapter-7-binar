'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    
     await queryInterface.bulkInsert('msusers', 
      [
       {
        iduser: 'ADM001', 
        name: 'Jonathan',
        password : '123',
        level:'ADMIN',
        status:'ACTIVE',
        createdAt:null,
        updatedAt:null,
       },
       {
        iduser: 'P0001', 
        name: 'Eduardus',
        password : '123',
        level:'USER',
        status:'ACTIVE',
        createdAt:null,
        updatedAt:null,
       },
       {
        iduser: 'P0002', 
        name: 'Jonathan Eduardus',
        password : '123',
        level:'USER',
        status:'ACTIVE',
        createdAt:null,
        updatedAt:null,
       },
      ], 
    
    
    {});
    
  },

  down: async (queryInterface, Sequelize) => {
    
     await queryInterface.bulkDelete('msusers', null, {});
     
  }
};
