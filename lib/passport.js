const passport = require('passport');
const { Strategy: JWTStrategy, ExtractJwt } = require('passport-jwt');

const { msuser } = require('../models');

const options = {
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey: 'Jonathan',
}

passport.use(new JWTStrategy(options, async(payload, done) => {
    msuser.findByPk(payload.id)
        .then(msuser => done(null, msuser))
        .catch(err => done(err, false))
}))

module.exports = passport;