const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const { msuser } = require('../models');

async function authenticate(name, password, done){
    try {
       
        const user = await msuser.authenticate({ name, password})

        return done(null, user)

    } catch(err){
        
        return done(null, false, {message: err.message})
    
    }
}

passport.use(
    new LocalStrategy({
        nameField: 'name',
        passwordField: 'password'
    }, authenticate)
)

passport.serializeUser(
    (msuser, done) => done(null, msuser.id)
)

passport.deserializeUser(async (id, done) => {
        const user = await msuser.findByPk(id);
        return done(null, user);
    }
)

module.exports = passport;