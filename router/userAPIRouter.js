// Import router
const express = require('express');
const router = express.Router(); 

//restrict Local
const restrict = require('../middleware/restrictLocal');


//restrict jwt
//const restrict = require('../middleware/restrict');

const { indexUserAPI, createUserAPI, deleteUserAPI, updateUserAPI } = require('../controllers/userAPIController');

const auth = require ('../controllers/loginController' );


// Homepage
router.get('/', (req, res) => res.render ('index' ));

// Register user page
router.get('/register' , (req, res) => res.render ('register' ));
router.post('/register' , auth.registerUser );

// login user page

router.get('/login', (req, res) => res.render('loginUser'));
router.post('/login', auth.loginUser);


router.get('/whoami', restrict, auth.whoami);



//view or read
router.get('/api/view', indexUserAPI);

router.post('/api/create', createUserAPI);

router.get('/api/delete/:iduser', deleteUserAPI);

router.post('/api/update/updateuser/:iduser', updateUserAPI);

module.exports = router;

