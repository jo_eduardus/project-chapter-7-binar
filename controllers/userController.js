const { msuser } = require('../models');


module.exports = {

    indexUser: (req, res) => {

        if (req.session.loggedIn) {

            res.render('create')
    
        } else {
    
            res.redirect('/login')
    
        }

    },
    createUser: (req, res) => {

      const idUser = req.body.iduser;
      const nameUser = req.body.nameuser;
      const password = req.body.password;
      const level = req.body.level;
      const status = req.body.status;  

      if ( idUser !== "" && nameUser  !== "" && password !== "" && level !== "" && status !== "" ) {
        
        msuser.create ({

          iduser: idUser,
          name: nameUser,
          password: password,
          level:level,
          status:status,
          createdAt: new Date(),
          updatedAt: new Date()
          
        }).then(msuser => res.redirect('/view') );

        //res.status(200).send(`Welcome back ${idUser}`);
        //res.render('homeafterlogin', {idUser}  )
        
        } else {

          res.status(200).send(`Unsuccessful create data ${idUser}`);
          

        } ;


    },

    viewUser: (req, res) => {

      if (req.session.loggedIn) {

        msuser.findAll().then(msuser => {
        
          res.render('view', { msuser });

        });

      } else {

        res.redirect('/login')

      }


    },

    deleteUser: (req , res) => {

      if (req.session.loggedIn) {

        const idUser = req.params.iduser;

        msuser.destroy ({

          where:{
              iduser: idUser
          },
      
        }).then ( res.redirect('/view') )

      } else {

        res.redirect('/login')

      }


    },

    updateUserGet: (req, res) => {

      if (req.session.loggedIn) {

        const idUser =  req.params.iduser

        msuser.findOne({ where: {iduser: idUser} }).then( 

        msuser => {

          res.render('update', {

            iduser : idUser,
            nameuser : msuser.name,
            level : msuser.level,
            status : msuser.status,
            password : msuser.password
    
          })

        })

      } else {

        res.redirect('/login')

      }

    },

    updateUserPost: (req, res) => {

      //const convertData = parseInt(req.params.iduser);
      //const idUser = convertData.toString();
      const idUser = req.params.iduser;
      const nameUser = req.body.nameuser;
      const password = req.body.password;
      const level = req.body.level;
      const status = req.body.status;


      
      if ( idUser !== "" && nameUser  !== "" && password !== "" && level !== "" && status !== ""  ) {

        const condition = {

          where:{
      
            iduser: idUser
      
          }
      
        }
      
        msuser.update({
      
          name: nameUser,
          password: password,
          level:level,
          status:status,
          createdAt: new Date(),
          updatedAt: new Date()
      
        }, condition).then( res.redirect('/view') );

      } else {

        res.status(200).send(`Unsuccessful update data ${idUser}`);

      } ;


    }




};