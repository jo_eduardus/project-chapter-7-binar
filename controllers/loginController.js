//const dataUser = require('../db/user.json');
const { msuser } = require ('../models' );

//local auth
const passport = require('../lib/passportLocal');


//jwt auth
//const passport = require('../lib/passport');


/*jwt version
function format(user){
  const {id, name} = user;
  return {
      id,
      name,
      accessToken: user.generateToken()
  }
}
*/

module.exports = {

    /*
    loginForm: (req, res) => {
      res.render('login');
    },
    
    loginIndex: (req, res) => {

        if (req.session.loggedIn) {

            res.render('home')
    
          } else {
    
            res.redirect('/login')
    
          }

    },

    loginSuccess: (req, res) => {

        if (req.session.loggedIn) {

            res.render('homeafterlogin')
    
          } else {
    
            res.redirect('/login')
    
          }

    },

    loginCheck: (req, res ) => {

        const idUser = req.body.username;
        const passwordUser = req.body.password;  
  
        if ( (idUser === dataUser[0].username && passwordUser === dataUser[0].password) || ( idUser === dataUser[1].username && passwordUser === dataUser[1].password) ) {
        
        //res.status(200).send(`Welcome back ${idUser}`);
        req.session.loggedIn = true;
        res.render('homeafterlogin', {idUser}  )
        
        } else {

        res.redirect('login')
        //res.status(200).send(`Access Denied for ${idUser}`);

        };

    } */


    /*
    
    createUser: (req, res) => {

      const idUser = req.body.iduser;
      const nameUser = req.body.nameuser;
      const password = req.body.password;
      const level = req.body.level;
      const status = req.body.status;  

      if ( idUser !== "" && nameUser  !== "" && password !== "" && level !== "" && status !== "" ) {
        
        msuser.create ({

          iduser: idUser,
          name: nameUser,
          password: password,
          level:level,
          status:status,
          createdAt: new Date(),
          updatedAt: new Date()
          
        }).then(msuser => res.redirect('/view') );

        //res.status(200).send(`Welcome back ${idUser}`);
        //res.render('homeafterlogin', {idUser}  )
        
        } else {

          res.status(200).send(`Unsuccessful create data ${idUser}`);
          

        } ;


    }

    */

      registerUser: (req, res, next) => {
        const nameUser = req.body.name;
        const password = req.body.password;
        const level = req.body.level;
        const status = req.body.status;
      
        //Kita panggil static method register yang sudah kita buat tadi
        msuser.registerUser ({

            iduser: "USER" + nameUser,
            name: nameUser,
            password: password,
            level:level,
            status:status,
            createdAt: new Date(),
            updatedAt: new Date()


        })
        .then(() => {
        res.redirect ('/login' )
        })
        .catch(err => next(err))
      },

    loginUser: passport.authenticate('local', {
        successRedirect: '/',
        failureRedirect: '/login',
        failureFlash: true
    }),

    whoami: (req, res) => {
      console.log('req.user.dataValues', req.name)
      res.render('whoami', req.name.dataValues);
    }

 

/*jwt version
  loginUser: async (req, res) => {
    User.authenticate(req.body)
        .then(user => {
            res.json(
                format(user)
            )
        })
        .catch(err => res.json({
            message: err
        }))
  },
  whoami: (req, res) => {
    const currentuser = req.user;
    res.json(currentuser);
  }
*/

};