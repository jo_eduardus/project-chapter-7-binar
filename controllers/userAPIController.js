const { msuser } = require('../models');

module.exports = {

    indexUserAPI: (req, res) => {

        msuser.findAll().then(msuser => {
            res.status(200).json(msuser)
          })

    },

    createUserAPI: (req, res) => {

        const idUser = req.body.iduser;
        const nameUser = req.body.nameuser;
        const level = req.body.level;
        const status = req.body.status;  
        const password = req.body.password;
        
    
        msuser.create ({

        iduser: idUser,
        name: nameUser,
        level: level,
        status: status,
        password: password,
        createdAt: new Date(),
        updatedAt: new Date()
        
        }).then(res.status(200).json(`Success created data, ${idUser}`) );

        res.status(200).json(`Unsuccessful created data, ${idUser}`)

    },

    deleteUserAPI: (req, res) => {

        const idUser = req.params.iduser;

        msuser.destroy ({

        where:{
            
            iduser: idUser

        }
    
        }).then ( res.status(200).json(`Success deleted data, ${idUser}`) );

        res.status(200).json(`Unsuccessful deleted data, ${idUser}`)

    },

    updateUserAPI: (req, res) => {

        const idUser = req.params.iduser;
        const nameUser = req.body.nameuser;
        const level = req.body.level;
        const status = req.body.status;
        const password = req.body.password;

        const condition = {

            where: {
        
                iduser: idUser
        
            }
    
        }
    
        msuser.update({
    
        name: nameUser,
        level: level,
        status: status,
        password: password,
        createdAt: new Date(),
        updatedAt: new Date()
    
        }, condition).then(res.status(200).json(`Success updated data, ${idUser}`) );

        res.status(200).send(`Unsuccessful updated data ${idUser}`);

    },


    

    




};
